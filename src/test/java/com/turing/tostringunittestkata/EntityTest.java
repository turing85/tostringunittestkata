package com.turing.tostringunittestkata;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;
import org.junit.jupiter.api.Test;

class EntityTest {

  @Test
  void shouldReturnExpectedValuesAfterConstructorIsCalled() {
    // GIVEN
    final UUID expectedId = UUID.randomUUID();
    final String expectedName = "John Doe";

    // WHEN
    final Entity entity = new Entity(expectedId, expectedName);

    // THEN
    final UUID actualId = entity.getId();
    assertEquals(expectedId, actualId, "ids do not match");
    final String actualName = entity.getName();
    assertEquals(expectedName, actualName, "names do not match");
  }

}