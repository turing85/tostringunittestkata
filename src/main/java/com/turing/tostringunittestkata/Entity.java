package com.turing.tostringunittestkata;

import java.util.UUID;

public class Entity {
  private static final String TO_STRING_TEMPLATE = "[id=%s,name=%s]";
  private final UUID id;
  private final String name;

  Entity(final UUID id, final String name) {
    this.id = id;
    this.name = name;
  }

  UUID getId() {
    return id;
  }

  String getName() {
    return name;
  }

  @Override
  public String toString() {
    return super.toString() + String.format(TO_STRING_TEMPLATE, getId(), getName());
  }
}