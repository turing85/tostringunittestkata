## toString() Unit Test [Kata][Kata]

The goal of this kata is to get the line-coverage of the unit under test (`Entity`) to above 90%. 
Since constructor and getters are already tested, the only method left to test is `toString()`. 

The following are the rules as intended by the author:

1. To solve this kata, one is only allowed to modify the file `src/test/java/com/turing/tostringunittestkata/EntityTest.java`.
2. The test case for the `toString()`-method should generate a random [`UUID`][UUID] and pass this as id to the constructor of `Entity`.
3. For the `name`-attribute, one may use a hardcoded value.
3. The test case should use [regular expressions][JavaRegex] to verify correct behaviour of the unit under test.
4. Bonus challenge: generate a random `String` without the help of an external library and pass is as `name`-attribute to the constructor of `Entity`.

While intended to practice the use of regex in Java, the kata can be solved however you want.

[Kata]: https://en.wikipedia.org/wiki/Kata_(programming)
[UUID]: https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/util/UUID.html
[JavaRegex]: https://docs.oracle.com/javase/tutorial/essential/regex/ 